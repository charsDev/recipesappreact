# Configuracion
* Instalamos dependencias
* Configuramos archivo __index.html__
* Creamos los archivos __(algo).config.js__ y los configuramos
* Creamos carpeta __src__
  * Creamos el archivo de entrada __entries\home.js__. Este archvio lo vinculamos en nuestro index.html
  * Creamos el archvi __pages\containers\homeContainer.js__ el cual renderiza un Hola Mundo
* Archivo .gitignore para evitar subir archivos innecesarios

# Creando el Title
* En el archivo __index.html__
  * Agregamos fuente Lato
* Creamos componente __title.js__
  * Retorna un contenedor con una etiqueta p y el texto Recipe App
* Creamos los estilos __title.css__
* Creamos el componente __homeLayout.js__
  * Retornamos los props.children dentro de un contenedor con la clase HomeLayout

# Creando el Search(UI)
* Creamos el componente __search.js__
  * Creamos una funcion Search que retorna un div con el input y el button
  * Importamos los estilos
* En el componente __homeContainer.js__
  * Importamos el componente Search
  * Agreagamos el componente Search dentro del HomeLayout

# Cambiando el store con redux, redux-thunk
* Cambiamos la manera en obtener los datos de inicio

# Configurando el reset store y el search
* Quedo configurado el reset store y el search

<!-- Falta darle forma para pantallas mas grandes, solo funciona en cels -->