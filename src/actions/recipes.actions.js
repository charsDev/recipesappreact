export const FETCH_RECIPES  = 'FETCH_RECIPES'
export const RESET_RECIPES = 'RESET_RECIPES'
export const SEARCH_RECIPES = 'SEARCH_RECIPES'

export const getAllRecipes = () => {
  return (dispatch) => {
    fetch('http://food2fork.com/api/search?key=567adf89998327420159050c7b9a3047')
    .then(res => res.json())
    .then(recipes => {
      dispatch({
        type: FETCH_RECIPES,
        payload: recipes
      })
    })
  }
}

export const searchRecipes = (ingredient) => {
  return (dispatch) => {
    fetch(`https://www.food2fork.com/api/search?key=567adf89998327420159050c7b9a3047&q=${ingredient}`)
    .then(res => res.json())
    .then(recipes => {
      dispatch({
        type: FETCH_RECIPES,
        payload: recipes
      })
    })
  }
}

export const resetRecipes = () => {
  return (dispatch) => {
    dispatch({
      type: RESET_RECIPES
    })
  }
}

// Quedo pendiente hacer que se resetee el store cuando haga un search