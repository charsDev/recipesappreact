import React from 'react'
import { render } from 'react-dom'
import Home from '../pages/containers/homeContainer'

import { applyMiddleware, createStore } from 'redux'
import { Provider } from 'react-redux'
import rootReducer from '../reducers/index'
import thunkMiddleware from 'redux-thunk'
import logger from 'redux-logger'

const home = document.getElementById('home-container')

const store = createStore(
  rootReducer,
  applyMiddleware(logger, thunkMiddleware)
)
render(
<Provider store={store}>
    <Home />
  </Provider>,
  home
)
