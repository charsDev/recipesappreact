import React from 'react'
import './homeLayout.css'

const HomeLayout = props => (
  <div className="HomeLayout">
    {props.children}
  </div>
)

export default HomeLayout