import React, { Component } from 'react'
import HomeLayout from '../components/homeLayout'
import Title from '../../title/components/title'
import Recipes from '../../recipes/components/recipes'
import { connect } from 'react-redux'
import { getAllRecipes } from '../../actions/recipes.actions'
import { bindActionCreators } from 'redux';

class Home extends Component {

  componentDidMount = () => {
    this.props.getAllRecipes()
  }

  render() {
    return (
      <HomeLayout>
        <Title />
        <Recipes recipesList={this.props.recipesList}/>
      </HomeLayout>     
    )
  }
}

function mapStateToProps(state) {
  return {
    recipesList: state.recipes
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getAllRecipes: bindActionCreators(getAllRecipes, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home) 