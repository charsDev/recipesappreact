import React, { Component } from 'react'
import Search from '../components/search'
import { connect } from 'react-redux'

import { resetRecipes, searchRecipes } from '../../actions/recipes.actions'
import { bindActionCreators } from 'redux';

class SearchContainer extends Component {
  state = {
    value: ''
  }

  handleSubmit = event => {
    event.preventDefault()
    this.props.resetRecipes()
    this.props.searchRecipes(this.state.value)
  }

  setInputRef = element => {
    this.input = element
  }

  handleInputChange = event => {
    this.setState({
      value: event.target.value
    })
  } 

  render() {
    return (
      <Search
        setRef = {this.setInputRef}
        handleSubmit = {this.handleSubmit}
        handleChange = {this.handleInputChange}
        value = {this.state.value} 
      />
    )
  }
}

function mapDispatchToProps(dispatch) {
  return {
    resetRecipes: bindActionCreators(resetRecipes, dispatch),
    searchRecipes: bindActionCreators(searchRecipes, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(SearchContainer)