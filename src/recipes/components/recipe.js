import React from 'react'
import './recipe.css'

const Recipe = (props) => (
  <div className="Recipe">
    <figure>
      <img src={props.recipe.image_url} alt=""/>
    </figure>
    <p className="Recipe-Title">{props.recipe.title.toUpperCase()}</p>
    <p className="Recipe-Publisher">
      <span>PUBLISHER:</span> {props.recipe.publisher}
    </p>
    <div className="Recipe-Detail">
      <a href={props.recipe.source_url}>
        View Recipe
      </a>
    </div>
  </div>
)

export default Recipe