import React from 'react'
import Recipe from './recipe'
import SearchContainer from '../../widgets/containers/SearchContainer'

const Recipes = (props) => (
  <div className="Recipes">
    <SearchContainer />
    {
      props.recipesList.map((recipe) => {
        return (
          <Recipe recipe={recipe} key={recipe.recipe_id}/>
        )
      })
    }
  </div>
)

export default Recipes