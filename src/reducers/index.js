import { combineReducers } from 'redux'

import recipes from './recipes.reducers'

const rootReducer = combineReducers({
  recipes
})

export default rootReducer