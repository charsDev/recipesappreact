import { FETCH_RECIPES, SEARCH_RECIPES, RESET_RECIPES } from '../actions/recipes.actions'

let initialState = []

export default (state = initialState, action) => {
  switch(action.type) {
    case FETCH_RECIPES:
      return [...state, ...action.payload.recipes]
    case SEARCH_RECIPES: 
      return [...state, ...action.payload.recipes]
    case RESET_RECIPES:
      return initialState
    default:
      return state
  }
}